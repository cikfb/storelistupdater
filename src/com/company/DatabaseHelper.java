package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DatabaseHelper creates a connection to the POSDEV1 database.
 */
public class DatabaseHelper
{
    // Path for the database driver
    private static final String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    // Connection data
    private static final String DB_CONNECTION =
            "jdbc:sqlserver://devsqlsrv1.wakefern.com;" +
            "user=DEVAPP1;" +
            "password=developmentTest1;" +
            "database=POSDEV1";

    /**
     * Creates Connection to a Database.
     * @return the Connection that was made to the POSDEV1 database, or
     * null if an error occurred while the Connection was being made.
     * @throws SQLException if a Connection error occurs.
     */
    public static Connection connect() throws SQLException
    {
        try
        {
            // Preloads the sql server driver
            Class.forName(DB_DRIVER);

            // Returns a Connection to the database
            return DriverManager.getConnection(DB_CONNECTION);
        }
        catch (ClassNotFoundException ex)
        {
            System.err.println("Could not find sqljdbc.jar. " + ex.getMessage());
            return null;
        }
    }
}