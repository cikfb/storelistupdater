package com.company;

/**
 * ContactsData holds variables that correlate to each column in the contacts list.
 * Every contact has its own ContactsData object.
 */
public class ContactsData
{
    int chainNumber;
    String lastName;
    String firstName;
    String email;
    String officeNumber;
    String cellNumber;
    String extension;
}
