package com.company;

import java.sql.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * StoreListUpdater calls a set of stored procedures that update the dbo.STORES
 * table with the most up-to-date changes.
 */
public class StoreListUpdater
{
    private static final String STORE_EXISTS_QUERY = "{call sp_STORE_Exists(?)}";
    private static final String STORE_INSERT_CALL = "{call sp_STORE_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
    private static final String STORE_UPDATE_CALL = "{call sp_STORE_LimitedUpdate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

    public static void main(String[] args)
    {
        // Parses the store list into a hash map (Key = Store Number, Value = Object containing fields)
        HashMap<Integer, StoreData> stores = Parser.readStoreData();

        // Creates iterator for hash map
        Iterator storeIterator = stores.entrySet().iterator();

        // Iterates through each store in the hash map
        while (storeIterator.hasNext())
        {
            Map.Entry store = (Map.Entry) storeIterator.next();

            // Gets key and value from map entry
            int storeNumber = (int) store.getKey();
            StoreData storeData = (StoreData) store.getValue();


            System.out.println(storeNumber);
            System.out.println("\tchainName: " + storeData.chainName);
            System.out.println("\tcity: " + storeData.city);
            System.out.println("\tcounty: " + storeData.county);
            System.out.println("\tfaxNumber: " + storeData.faxNumber);
            System.out.println("\topenedScheduled: " + storeData.openedScheduled);
            System.out.println("\tphoneNumber: " + storeData.phoneNumber);
            System.out.println("\trxFaxNumber: " + storeData.rxFaxNumber);
            System.out.println("\trxPhoneNumber: " + storeData.rxPhoneNumber);
            System.out.println("\tstate: " + storeData.state);
            System.out.println("\tstreetAddress: " + storeData.streetAddress);
            System.out.println("\tzip: " + storeData.zip);
            System.out.println("\tchainNumber: " + storeData.chainNumber);
            System.out.println("\tzone: " + storeData.zone);
            System.out.println();


            try (
                    Connection connection = DatabaseHelper.connect();
                    CallableStatement callableStatement_Exists = connection.prepareCall(STORE_EXISTS_QUERY);
                    CallableStatement callableStatement_Insert = connection.prepareCall(STORE_INSERT_CALL);
                    CallableStatement callableStatement_Update = connection.prepareCall(STORE_UPDATE_CALL);
            ){
                callableStatement_Exists.setInt("storeNumber", storeNumber);

                try
                {
                    // Determines if the current store exists in database
                    ResultSet rs = callableStatement_Exists.executeQuery();
                    rs.next();
                    String exists = rs.getString("");

                    if (exists.equals("TRUE"))
                    {
                        // Update the existing store with new data
                        // Adds parameters to callable statement
                        callableStatement_Update.setInt("storeNumber", storeNumber);
                        if (storeData.zone == -1)
                        {
                            callableStatement_Update.setNull("zone", Types.SMALLINT);
                        }
                        else
                        {
                            callableStatement_Update.setInt("zone", storeData.zone);
                        }
                        callableStatement_Update.setString("storeName", storeData.storeName);
                        callableStatement_Update.setString("openedScheduled", storeData.openedScheduled);
                        callableStatement_Update.setString("chainName", storeData.chainName);
                        if (storeData.chainNumber == -1)
                        {
                            callableStatement_Update.setNull("chainNumber", Types.SMALLINT);
                        }
                        else
                        {
                            callableStatement_Update.setInt("chainNumber", storeData.chainNumber);
                        }
                        callableStatement_Update.setString("streetAddress", storeData.streetAddress);
                        callableStatement_Update.setString("city", storeData.city);
                        callableStatement_Update.setString("county", storeData.county);
                        callableStatement_Update.setString("state", storeData.state);
                        callableStatement_Update.setString("zip", storeData.zip);
                        callableStatement_Update.setString("phone", storeData.phoneNumber);
                        callableStatement_Update.setString("fax", storeData.faxNumber);
                        callableStatement_Update.setString("rxPhone", storeData.rxPhoneNumber);
                        callableStatement_Update.setString("rxFax", storeData.rxFaxNumber);

                        callableStatement_Update.execute();
                    }
                    else
                    {
                        // Insert new store into database
                        // Adds parameters to callable statement
                        callableStatement_Insert.setInt("storeNumber", storeNumber);
                        if (storeData.zone == -1)
                        {
                            callableStatement_Insert.setNull("zone", Types.SMALLINT);
                        }
                        else
                        {
                            callableStatement_Insert.setInt("zone", storeData.zone);
                        }
                        callableStatement_Insert.setString("storeName", storeData.storeName);
                        callableStatement_Insert.setString("openedScheduled", storeData.openedScheduled);
                        callableStatement_Insert.setString("chainName", storeData.chainName);
                        if (storeData.chainNumber == -1)
                        {
                            callableStatement_Insert.setNull("chainNumber", Types.SMALLINT);
                        }
                        else
                        {
                            callableStatement_Insert.setInt("chainNumber", storeData.chainNumber);
                        }
                        callableStatement_Insert.setString("streetAddress", storeData.streetAddress);
                        callableStatement_Insert.setString("city", storeData.city);
                        callableStatement_Insert.setString("county", storeData.county);
                        callableStatement_Insert.setString("state", storeData.state);
                        callableStatement_Insert.setString("zip", storeData.zip);
                        callableStatement_Insert.setString("phone", storeData.phoneNumber);
                        callableStatement_Insert.setString("fax", storeData.faxNumber);
                        callableStatement_Insert.setString("rxPhone", storeData.rxPhoneNumber);
                        callableStatement_Insert.setString("rxFax", storeData.rxFaxNumber);

                        // Sets unknown parameters to null
                        callableStatement_Insert.setNull("veribalance", Types.BIT);
                        callableStatement_Insert.setNull("mom", Types.BIT);
                        callableStatement_Insert.setNull("mobile", Types.BIT);
                        callableStatement_Insert.setNull("lanehawk", Types.BIT);
                        callableStatement_Insert.setNull("tomra", Types.BIT);
                        callableStatement_Insert.setNull("fe", Types.TINYINT);
                        callableStatement_Insert.setNull("rap", Types.TINYINT);
                        callableStatement_Insert.setNull("cs", Types.TINYINT);
                        callableStatement_Insert.setNull("rx", Types.TINYINT);
                        callableStatement_Insert.setNull("fc", Types.TINYINT);
                        callableStatement_Insert.setNull("ls", Types.TINYINT);
                        callableStatement_Insert.setNull("srfh", Types.TINYINT);
                        callableStatement_Insert.setNull("sat", Types.TINYINT);
                        callableStatement_Insert.setNull("ncr", Types.TINYINT);
                        callableStatement_Insert.setNull("mr", Types.TINYINT);
                        callableStatement_Insert.setNull("css", Types.TINYINT);
                        callableStatement_Insert.setNull("app", Types.TINYINT);
                        callableStatement_Insert.setNull("ms", Types.TINYINT);
                        callableStatement_Insert.setNull("totals", Types.TINYINT);
                        callableStatement_Insert.setNull("laneNumbers", Types.NVARCHAR);
                        callableStatement_Insert.setNull("openeddate", Types.DATE);
                        callableStatement_Insert.setNull("ipaddress", Types.NVARCHAR);

                        // Inserts new store
                        callableStatement_Insert.execute();
                    }
                }
                catch (SQLException e)
                {
                    System.out.println("Call unsuccessful!");
                    e.printStackTrace();
                }
            }
            catch (SQLException e)
            {
                System.out.println("Connection unsuccessful!");
                e.printStackTrace();
            }
        }
    }
}
