package com.company;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * ContactsUpdater calls a set of stored procedures that update the dbo.STORES_CONTACTS
 * table with the most up-to-date changes.
 */
public class ContactsUpdater
{
    private static final String CONTACT_EXISTS_QUERY = "{call sp_STORES_CONTACTS_Exists(?,?,?)}";
    private static final String CONTACT_UPDATE_CALL = "{call sp_STORES_CONTACTS_Update(?,?,?,?,?,?,?)}";
    private static final String CONTACT_INSERT_CALL = "{call sp_STORES_CONTACTS_Add(?,?,?,?,?,?,?)}";
    private static final String CONTACT_UPDATE_CHAIN_NAMES = "{call sp_STORES_CONTACTS_Update_ChainNames()}";

    public static void main(String[] args)
    {
        ArrayList<ContactsData> contacts = Parser.readContactsData();

        // Iterates through each of the contacts
        for (int i = 0; i < contacts.size(); i++)
        {
            ContactsData contact = contacts.get(i);

            /*
            System.out.println("chainNumber: " + contact.chainNumber);
            System.out.println("firstName: " + contact.firstName);
            System.out.println("lastName: " + contact.lastName);
            System.out.println("email: " + contact.email);
            System.out.println("extension: " + contact.extension);
            System.out.println("officeNumber: " + contact.officeNumber);
            System.out.println("cellNumber: " + contact.cellNumber);
            System.out.println("\n");
             */

            try (
                    Connection connection = DatabaseHelper.connect();
                    CallableStatement callableStatement_Exists = connection.prepareCall(CONTACT_EXISTS_QUERY);
                    CallableStatement callableStatement_Insert = connection.prepareCall(CONTACT_INSERT_CALL);
                    CallableStatement callableStatement_Update = connection.prepareCall(CONTACT_UPDATE_CALL);
            ) {
                // Sets parameters for query that checks if the current contact exists
                callableStatement_Exists.setInt("chainNumber", contact.chainNumber);
                callableStatement_Exists.setString("firstName", contact.firstName);
                callableStatement_Exists.setString("lastName", contact.lastName);

                try
                {
                    // Determines if the current store exists in database
                    ResultSet rs = callableStatement_Exists.executeQuery();
                    rs.next();
                    String exists = rs.getString("");

                    if (exists.equals("TRUE"))
                    {
                        // Update the existing store with new data
                        // Adds parameters to callable statement
                        callableStatement_Update.setInt("chainNumber", contact.chainNumber);
                        callableStatement_Update.setString("firstName", contact.firstName);
                        callableStatement_Update.setString("lastName", contact.lastName);
                        callableStatement_Update.setString("email", contact.email);
                        callableStatement_Update.setString("phone", contact.officeNumber);
                        callableStatement_Update.setString("ext", contact.extension);
                        callableStatement_Update.setString("cell", contact.cellNumber);

                        // Executes update
                        callableStatement_Update.execute();
                    }
                    else
                    {
                        // Insert new store into database
                        // Adds parameters to callable statement
                        callableStatement_Insert.setInt("chainNumber", contact.chainNumber);
                        callableStatement_Insert.setString("firstName", contact.firstName);
                        callableStatement_Insert.setString("lastName", contact.lastName);
                        callableStatement_Insert.setString("email", contact.email);
                        callableStatement_Insert.setString("phone", contact.officeNumber);
                        callableStatement_Insert.setString("ext", contact.extension);
                        callableStatement_Insert.setString("cell", contact.cellNumber);

                        // Inserts new store
                        callableStatement_Insert.execute();
                    }
                }
                catch(SQLException e)
                {
                    System.out.println("Call unsuccessful!");
                    e.printStackTrace();
                }
            }
            catch(SQLException e)
            {
                System.out.println("Connection unsuccessful!");
                e.printStackTrace();
            }
        }

        try (
                Connection connection = DatabaseHelper.connect();
                CallableStatement callableStatement_Update_Chain_Names = connection.prepareCall(CONTACT_UPDATE_CHAIN_NAMES);
        )
        {
            // Updates chain names
            callableStatement_Update_Chain_Names.execute();
        }
        catch(SQLException e)
        {
            System.out.println("Updating chain names unsuccesful!");
            e.printStackTrace();
        }
    }
}
