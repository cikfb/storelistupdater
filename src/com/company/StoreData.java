package com.company;

/**
 * StoreData holds variables that correlate to each column in the store list.
 * Every store has its own StoreData object.
 */
class StoreData
{
    public int zone;
    public String storeName;
    public String openedScheduled;
    public String chainName;
    public int chainNumber;
    public String streetAddress;
    public String city;
    public String county;
    public String state;
    public String zip;
    public String phoneNumber;
    public String faxNumber;
    public String rxPhoneNumber;
    public String rxFaxNumber;
}
