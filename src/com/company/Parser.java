package com.company;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Pqrser contains methods that read in and parse the files, StoreList.csv and , which
 * holds the most up-to-date information regarding the stores.
 */
public class Parser
{
    public static HashMap readStoreData()
    {
        // Each key in this hash map is a store number contained in the store list
        HashMap<Integer, StoreData> stores = new HashMap<>();

        try
        {
            // Creates scanner for the store list
            // Every comma in the file was replaced with a "`" character
            Scanner sc = new Scanner(new File("resources\\StoreList.csv"));

            // Skips past headers in the list
            for (int i = 0; i < 3; i++)
            {
                sc.nextLine();
            }

            // Iterates through each row
            while (sc.hasNext())
            {
                Scanner row = new Scanner(sc.nextLine() + ",");
                row.useDelimiter(",");

                String token = row.next();
                
                // Escapes if the current row is blank
                if (token.equals(""))
                {
                    continue;
                }

                // Removes new line character from token and parses the store number
                int storeNumber = Integer.parseInt(token);

                StoreData data = new StoreData();

                token = row.next();
                data.zone = token.equals("") ? -1 : Integer.parseInt(token);

                token = row.next();
                data.storeName = token.equals("") ? "--" : token.replace('`', ',');

                // Excludes non-stores from being added to database
                String storeName = data.storeName.toLowerCase();
                if (storeName.contains("distribution") ||
                    storeName.contains("commissary") ||
                    storeName.contains("warehouse") ||
                    storeName.contains("bakery") ||
                    storeName.contains("fuel"))
                {
                    row.close();
                    continue;
                }

                // Parses the store data column by column
                token = row.next();
                data.openedScheduled = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.chainName = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.chainNumber = token.equals("") ? -1 : Integer.parseInt(token);

                token = row.next();
                data.streetAddress = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.city = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.county = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.state = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.zip = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.phoneNumber = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.faxNumber = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.rxPhoneNumber = token.equals("") ? "--" : token.replace('`', ',');

                token = row.next();
                data.rxFaxNumber = token.equals("") ? "--" : token.replace('`', ',');

                // Closes row scanner and adds store to hash map
                row.close();
                stores.put(storeNumber, data);
            }

            // Closes the scanner
            sc.close();

            return stores;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<ContactsData> readContactsData()
    {
        ArrayList<ContactsData> contacts = new ArrayList<ContactsData>();

        try
        {
            // Creates scanner for the contacts list
            Scanner sc = new Scanner(new File("resources\\ContactsList.csv"));

            // Skips past headers in the list
            sc.nextLine();

            // Iterates through each row
            while (sc.hasNext())
            {
                Scanner row = new Scanner(sc.nextLine() + ",");
                row.useDelimiter(",");

                // Skips past chain name
                row.next();

                String token = row.next().trim();

                // Escapes if the current row is blank
                if (token.equals(""))
                {
                    row.close();
                    continue;
                }

                ContactsData contactsData = new ContactsData();

                // Removes new line character from token and parses the chain number
                contactsData.chainNumber = Integer.parseInt(token);

                // Skips past chain coordinator
                row.next();

                token = row.next().trim();
                contactsData.lastName = token.equals("") ? "--" : token;

                token = row.next().trim();
                contactsData.firstName = token.equals("") ? "--" : token;

                // Skips past CISD CC Group
                row.next();

                token = row.next().trim();
                contactsData.email = token.equals("") ? "--" : token;

                token = trimPhoneNumber(row.next().trim());
                System.out.println(token);
                contactsData.officeNumber = token.equals("") ? "--" : (token.length() < 10 ? "" : token);

                token = row.next().trim();
                contactsData.extension = token.equals("") ? "--" : token;

                token = trimPhoneNumber(row.next().trim());
                System.out.println(token);
                contactsData.cellNumber = token.equals("") ? "--" : (token.length() < 10 ? "" : token);

                // Closes row scanner and adds contact to list
                row.close();
                contacts.add(contactsData);
            }

            // Closes the scanner
            sc.close();

            return contacts;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static String trimPhoneNumber(String phoneNumber)
    {
        return phoneNumber
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("-", "")
                .replaceAll("\\.", "")
                .replaceAll(" ", "");
    }
}
